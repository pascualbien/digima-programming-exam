// Warm-up (Number Squence)
// To check the solution, please comment out lines 4 - 11.

// let outputWarmUp = [];
// outputWarmUp.push(parseInt(prompt('Enter first number:')));
// outputWarmUp.push(parseInt(prompt('Enter second number:')));
// for (let i = 1; i < 9; i++) {
//     let arithmetic = outputWarmUp[i-1] + outputWarmUp[i];
//     outputWarmUp.push(arithmetic);
// }
// console.log(outputWarmUp);

// Easy (Decimal to Binary)
// To check the solution, please comment out lines 16 - 27.

// let outputEasy = [];
// let inputEasy = parseInt(prompt('Input decimal number'));
// let iteration = inputEasy;

// for (let i = 0; i < iteration; i++) {
//     if (inputEasy > 0) {
//         outputEasy.push(inputEasy%2);
//         inputEasy = Math.floor(inputEasy / 2);
//     }
// }
// outputEasy.reverse();
// console.log(outputEasy);

// Medium (Prime Factors)
// To check the solution, please comment out lines 32 - 70.

// let outputMedium = [];
// let inputMedium = parseInt(prompt('Prime factors of a number:'));
// let iteration = inputMedium;

// function primeNumberTest(num) {
//     // by law of numbers, 1 is not a prime number since a prime number by definition has only two positive factor.
//     if (num === 1) {
//         return false;
//     // two is a prime number since it has only two factors.
//     } else if (num === 2) {
//         return true;
//     } else {
//         // loop for checking if the number is divisible by 2, 3, 4 etc. We don't need 1 since it is the one factor and the number itself for it to be a prime factor.
//         for (let i = 2; i < num; i++) {
//             if (num % i  === 0) {
//                 return false;
//             } 
//         }
//         // we need to return true after the loop to make sure that after looping the number in the testing condition which is num % i === 0 and it did not pass, meaning it's a prime number, therefore we return true.
//         return true;
//     }
// }

// if (!primeNumberTest(inputMedium)) {
//     for (let i = 2; i < iteration; i++) {
//         if (inputMedium > 0) {
//             for (let i = 2; i < iteration; i++) {
//                 if (primeNumberTest(i) && inputMedium % i === 0) {
//                     outputMedium.push(i);
//                     inputMedium = inputMedium / i;
//                 }
//             }         
//         }
//     }
// } else {
//     outputMedium.push(parseInt('1'));
//     outputMedium.push(parseInt(inputMedium));
// }
// console.log(outputMedium.sort());

// Medium (SquareRoot)
// Incomplete answer
// let squareRootInput = prompt(parseInt('Take the square root of a number'))
// function squareRoot(number) {
//     if (number <= 0) {
//         alert('Invalid output, please make sure that the input is integer.')
//     } else {

//     }
// }